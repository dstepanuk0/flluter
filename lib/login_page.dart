// import 'dart:html';

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'signe_in.dart';
import 'dart:ui';

// import 'package:flutter_my_project/signe_in.dart';
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class BarCode {
  String name;

  String barCode;

  List<String> comment;

  void setName(String name) {
    this.name = name;
  }

  void setBarCode(String bareCode) {
    this.barCode = bareCode;
  }

  void setComment(String comment, BarCode bar) {
    bar.comment.add(comment);
  }

  String getName() {
    return this.name;
  }

  String getBarCode() {
    return this.barCode;
  }

  List<String> getComment() {
    return this.comment;
  }


}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  FirstScreen createState() => FirstScreen();
}

class FirstScreen extends State<MyHomePage> {
  List<BarCode> bareCode = List<BarCode>();

  showMoreInf(BuildContext context, int index) {
    TextEditingController controllerForComment = TextEditingController();
    int size;

    showDialog(
        context: context,
        builder: (_) =>
        new AlertDialog(
            title: new Text(
              "Bar code",
              textAlign: TextAlign.center,
            ),
            content: Column(
              children: <Widget>[
                Text(bareCode[index].barCode),
                // Text("Comments"),
              ],
              //     TextField(
              //       controller: controllerForComment,
              //     ),
              //   ],
              // ),
              // actions: <Widget>[
              //   FlatButton(
              //     child: Text('send'),
              //     // setState((){}),
              //     onPressed: () {
              //       setState(() {
              //         bareCode[index].setComment(controllerForComment.text,bareCode[index]);
              //       });
              //       Navigator.of(context).pop();
              //     },
              //   )
              // ],
            )),);
  }

  _showMaterialDialog(BuildContext context) {
    TextEditingController controllerForName = TextEditingController();
    TextEditingController controllerForCode = TextEditingController();

    showDialog(
        context: context,
        builder: (_) =>
        new AlertDialog(
          title: new Text(
            "Bar code",
            textAlign: TextAlign.center,
          ),
          content: Column(
            children: <Widget>[
              Text("Input code"),
              TextField(
                controller: controllerForCode,
              ),
              Text("Input name"),
              TextField(
                controller: controllerForName,
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Create'),
              // setState((){}),
              onPressed: () {
                setState(() {
                  BarCode barCode2 = BarCode();
                  barCode2.setBarCode(controllerForCode.text);
                  barCode2.setName(controllerForName.text);
                  bareCode.add(barCode2);
                });
                Navigator.of(context).pop();
              },
            )
          ],
        ));
  }
//ik
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your bar code'),
      ),
      body: ListView.builder(
          itemCount: bareCode.length,
          itemBuilder: (context, index) {
            return Card(
              borderOnForeground: true,
              child: ListTile(
                onTap: () {
                  return showMoreInf(context, index);
                },
                title: Text("Code: " + bareCode[index].barCode),
                subtitle: Text("Name: " + bareCode[index].name),
              ),
            );
          }),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          return _showMaterialDialog(context);
        },
        tooltip: 'Create code',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlutterLogo(size: 150),
              SizedBox(height: 50),
              _signInButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _signInButton() {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () {
        signInWithGoogle().then((result) {
          if (result != null) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return MyHomePage();
                },
              ),
            );
          }
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.grey),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage("assets/google_logo.png"), height: 30.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'Sign in with Google',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
